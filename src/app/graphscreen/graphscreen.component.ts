import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { BaseChartDirective } from 'ng2-charts';

// import { BaseChartDirective } from 'ng2-charts';
@Component({
  selector: 'app-graphscreen',
  templateUrl: './graphscreen.component.html',
  styleUrls: ['./graphscreen.component.css']
})
export class GraphscreenComponent implements OnInit {
  wsdata: any;


  constructor() { }
  private serverUrl = 'http://54.156.158.118:9080/kcl-websocket/1';
    private title = 'WebSockets chat';
    private stompClient;

    @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;

    public lineChartData: any[] = [
      {
        data: [], label: 'Load (Grams)'
      }
      

    ];

    public lineChartData2: any[] = [
     
      {
        data: [], label: 'Spread (Percentage)'
      }

    ];

  public barChartDataConc: any[] = [
    { data: ['40', '43', '67'], label: 'Iron Feed' },
    { data: ['40', '43', '67'], label: 'Silica Feed' }
];

public lineChartColors: Array<any> = [
  {
            backgroundColor: '#949FB1',
            borderColor: '#016191',
            pointBackgroundColor: '#016191',
            pointBorderColor: '#016191',
            pointHoverBackgroundColor: '#016191',
            pointHoverBorderColor: '#016191'
  }
];


public barChartType = 'bar';
public lineChartType = 'line';
public lineChartLabels: string[] = [];
public barChartLabels: string[] = ['1', '2', '4'];
public barChartLegend: any = true;
public lineChartLegend: any = true;
public lineChartOptions: any = {
  maintainAspectRatio: false,
  animation: {
      duration: 0
  },
  legend : {
      labels : {
        fontColor : '#000000'  ,
        fontSize: 8

      }
  },

  responsive: true
};
chartHovered($event) {

}

chartClicked($event) {

}

chartHovered1($event) {

}

chartClicked1($event) {

}


public callWebSocket() {
  const ws = new SockJS(this.serverUrl);
  this.stompClient = Stomp.over(ws);
  this.stompClient.debug = null;
  const that = this;

  // tslint:disable-next-line:only-arrow-functions
  this.stompClient.connect({}, function(frame) {

  });

  ws.onmessage = (e) => {
    this.wsdata = JSON.parse(e.data);
   // console.log(Number(this.wsdata.DATA));

      // var _this = this;
    const today = new Date();

    const sec = today.getSeconds();
    const millisec = today.getMilliseconds();

    const date = sec + ':' + millisec;

    this.wsdata = JSON.parse(e.data);
    this.lineChartLabels.push(date);
    this.lineChartData[0].data.push(Number(this.wsdata.DATA));
    this.lineChartData2[0].data.push(Number(this.wsdata.PERCENTOFBLUE) );
    if (this.lineChartData[0].data.length > 200) {
      this.lineChartData[0].data.shift();
      this.lineChartData2[0].data.shift();
      this.lineChartLabels.shift();
    }
    this.updateChart();

  };
}
private updateChart() {
  this.charts.forEach((child) => {
      child.chart.update();
  });
}


  ngOnInit() {
    this.callWebSocket();
  }

}
