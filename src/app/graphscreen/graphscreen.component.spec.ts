import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphscreenComponent } from './graphscreen.component';

describe('GraphscreenComponent', () => {
  let component: GraphscreenComponent;
  let fixture: ComponentFixture<GraphscreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
