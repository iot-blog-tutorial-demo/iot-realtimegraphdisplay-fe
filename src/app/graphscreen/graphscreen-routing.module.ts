import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {GraphscreenComponent} from './graphscreen.component';

const routes: Routes = [
  {
    path: '',
    component: GraphscreenComponent
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GraphscreenRoutingModule {}